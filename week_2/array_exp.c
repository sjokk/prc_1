#include <stdio.h>

#define ARRAY_SIZE 5

int arraySum(int *number, int size);

int arraySum(int *number, int size)
{
	int sum = 0;
	
	for (int i = 0; i < size; i++) {
		sum += *number;
		number++; // move on one reference in memory.
		
		printf("Sum currently = %d", sum);
		printf("\n");
	}
	
	return sum;
}

/*
 * Makes a new array containing numbers and otherNumbers appended.
 *  
 */
int *add(int *numbers, int *otherNumbers)
{
	
	return 
}

int main()
{
	int sumArray[ARRAY_SIZE] = { 5, 5, 5, 5, 5};
	
	// The name of the array can be passed as it points to the first
	// reference of the array.
	return arraySum(sumArray, ARRAY_SIZE);
}
