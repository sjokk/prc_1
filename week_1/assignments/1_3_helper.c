#include <stdio.h>

int factorial(int n)
{
	int result = 1;
	
	for (int i = 2; i <= n; i++)
	{
		result = result * i;
	}
	
	return result;
}

void printNewLine(int numberOfLines)
{
	for (int i = 0; i < numberOfLines; i++)
	{
		printf("\n");
	}
}
