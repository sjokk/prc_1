#include <stdio.h>
#include "1_3_helper.h"

void printFactorialSequence(int n);

int main()
{
	printFactorialSequence(5);
	
	return 0;
}

void printFactorialSequence(int n)
{
	int result;
	char printMessage[] = "The factorial of 0 and 1 = ";
	
	for (int i = 0; i <= n; i++)
	{
		result = factorial(i);
		
		if (i > 1)
		{
			// change message
			
		}
		printf("The factorial of %d = %d", i, result);
		printNewLine(1);
	}
	
	printNewLine(1);
}
