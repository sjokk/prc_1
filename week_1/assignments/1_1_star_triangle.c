#include <stdio.h>

void printStars(int numberOfStars);
void printStarTriangle(int numberOfSteps);

int main() 
{
	printStarTriangle(10);
	
	return 0;
}

void printStarTriangle(int numberOfSteps) {
	for (int i = 1; i <= numberOfSteps; i++) {
		printStars(i);
	}
}

void printStars(int numberOfStars)
{	
	for (int i = 0; i < numberOfStars; i++) {
		printf("*");
	}
	
	printf("\n");
}
