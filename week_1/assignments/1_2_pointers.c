#include <stdio.h>

int main()
{
	int x = 7;
	int *px1, *px2; 
	
	px1 = &x;
	px2 = &x;
	
	*px1 = 10;
	
	printf("X changed in value to: %d\n", *px2);
	
	return 0;
}
