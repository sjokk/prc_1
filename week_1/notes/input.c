#include <stdio.h>
#include <string.h>

int main()
{	
	int a;
	double f;
	char s; // Character arrays are strings. char s[10];
	
	// strcpy(s, "some string value");
	
	int d = 12;
	
	// Different input accepted through command line.
	scanf("%d", &a);
	scanf("%lf", &f);
	scanf("%s", &s);
	
	printf("%d\n", d);
	
	return 0;
}
