#include <stdio.h>

/*
 * int *xp;
 * int x = 7;
 * 
 * xp = &x;
 * *xp = 12; -> re-assigne the value of the pointer to 12, thus *xp is almost as if you directly access the memory location.
 * thus x is also 12 now since they refer to the same location in memory.
 * 
