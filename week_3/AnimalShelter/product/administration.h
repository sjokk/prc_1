#ifndef _ADMINISTRATION_H
#define _ADMINISTRATION_H

#include "animal.h"

#define ANIMAL_SHELTER_SIZE 3
#define NUMBER_OF_SPECIES 4

void initializeShelter(ANIMAL animalArray[]);
void showAnimals(ANIMAL animalArray[]);
int addAnimal (ANIMAL *AnimalPtr, ANIMAL animalArray[]);
void printAnimal(ANIMAL *animal);
char *getSpeciesName(int species);
int removeAnimalAt(int index, ANIMAL animalArray[]);
int findAnimal(char name[], ANIMAL animalArray[]);
void createDefaultAnimal(ANIMAL *animal);
int removeAllAnimalsBy(char name[], ANIMAL animalArray[]);
int getNumberOfAnimals(ANIMAL animalArray[]);
int isThereRoomForOneMore(ANIMAL animalArray[]);

#endif
