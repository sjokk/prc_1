#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "administration.h"

/*
 * Initializes the given array to contain default animal structures. 
 */
void initializeShelter(ANIMAL animalArray[])
{	
	ANIMAL defaultAnimal;
	createDefaultAnimal(&defaultAnimal);
	
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		animalArray[i] = defaultAnimal; 
	}
}

/*
 * Loops over the array of animals, checking if the name of the animal
 * is set.
 * 
 * If the name is set, it means we are dealing with a non default animal
 * and we print it's details.
 */
void showAnimals(ANIMAL animalArray[])
{
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		if (strlen(animalArray[i].Name) > 0)
		{	printf("%i: ", i + 1);
			printAnimal(&animalArray[i]);
		}
	}
	
	printf("\n");
}

/*
 * Adds the given animal to the given array, by checking if there
 * is an animal with default initialization (meaning there is a spot open)
 * otherwise it exits with -1 indicating failure to add.
 */
int addAnimal (ANIMAL *AnimalPtr, ANIMAL animalArray[])
{
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		if (strlen(animalArray[i].Name) == 0)
		{
			animalArray[i] = *AnimalPtr;
			return 0;
		}
	}
	
	return -1;
}

/*
 * Prints the details of the animal that was passed by reference.
 */
void printAnimal(ANIMAL *animal)
{
	// Will handle the formatting and printing of the animal.
	printf(
		"%s, is a %s age %i \n", 
		animal->Name, 
		getSpeciesName(animal->Species), 
		animal->Age
	);
}

/*
 * Returns the corresponding name of the species.
 */
char *getSpeciesName(int speciesInt)
{
	switch(speciesInt)
	{
		case 0:
			return "cat";
		case 1:
			return "dog";
		case 2:
			return "guinea pig";
		case 3:
			return "parrot";
		default:
			return "";
	}
}

/*
 * Tries to reset the animal structure at the given array to its default
 * values.
 * 
 * Returns 0 in case it successfully made changes and -1 otherwise.
 * 
 * Shifting is also not a true removal though, since the same problem
 * exists, it's approached differently though.
 */
int removeAnimalAt(int index, ANIMAL animalArray[])
{
	int successCode = -1;
	
	if (
		index >= 0 
		&& index < ANIMAL_SHELTER_SIZE \
		&& strlen(animalArray[index].Name) > 0
	)
	{
		int finalIndex = ANIMAL_SHELTER_SIZE - 1;
		
		// We could break as soon as we encounter a default animal struct
		for (int i = index; i + 1 < ANIMAL_SHELTER_SIZE; i++) 
		{	
			// For efficiency we can break if we find a default animal
			// before we exhaust the array.
			if (strcmp(animalArray[i + 1].Name, "") == 0)
			{
				finalIndex = i;
				break;
			}
			
			// move i + 1 on top of i, shifting animals leftwards.
			animalArray[i] = animalArray[i + 1];		
		}
		
		createDefaultAnimal(&animalArray[finalIndex]);
		
		successCode = 0;
	}
	
	return successCode;
}

/*
 * Tries to find an animal that has given name as name.
 * 
 * Returns the index of the first encounter or -1 otherwise.
 */
int findAnimal(char name[], ANIMAL animalArray[])
{	
	if (strcmp(name, "") != 0)
	{
		for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
		{
			if (strcmp(name, animalArray[i].Name) == 0)
			{
				return i;
			}
		}
	}
	
	return -1;
}

/*
 * Assigns fields that are considered defaults for the animal structure.
 */
void createDefaultAnimal(ANIMAL *animal)
{
	strcpy(animal->Name, "");
	animal->Species = Cat;
	animal->Age = -1;
}

int removeAllAnimalsBy(char name[], ANIMAL animalArray[])
{
	int index;
	int successfullyRemoved;
	int numberOfRemovedAnimals;
	        
	do
	{
		index = findAnimal(name, animalArray);
		successfullyRemoved = removeAnimalAt(index, animalArray);
		 
		if (successfullyRemoved == 0) 
		{
			numberOfRemovedAnimals++;
		}
	 } while (successfullyRemoved == 0);
	 
	 return numberOfRemovedAnimals;
}

int getNumberOfAnimals(ANIMAL animalArray[])
{
	int total = 0;
	
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		if (strcmp(animalArray[i].Name, "") != 0)
		{
			total++;
		}
	}
	
	return total;
}


int isThereRoomForOneMore(ANIMAL animalArray[])
{
	return ANIMAL_SHELTER_SIZE - getNumberOfAnimals(animalArray) > 0;
}

