/*
 * Menu animal_shelter.c
 *
 *  
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "animal.h"
#include "administration.h"

ANIMAL getAnimal();
void getName(char *name);
int getSpecies();
int getAge();
void printMessageOnAdd(int successCode);
int getRemovalIndex();
void printMessageOnRemoval(int successCode);

int main (void)
{
	char name[MaxNameLength];
	
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
    printf ("PRC assignment 'Animal Shelter' (version april 2019)\n");
          
    int choice = -1;
    while (choice != 0)
    {
        printf ("\nMENU\n====\n");
        printf ("1: Show Animals \n");
        printf ("2: Add Animal \n");
        printf ("3: Remove Animal \n");
        printf ("4: Find Animal by name \n");
        printf ("5: # Animals in shelter \n");
        printf ("6: Is there room for one more animal \n");
        printf ("0: quit \n");
        
        scanf ("%d", &choice);
		printf("\n");
		
        switch (choice)
        {   
            case 1: //Show Animals
                printf ("show Animals\n");
                showAnimals(animalArray);
                break;
            case 2: //Add Animal
                printf ("add Animal\n");
                ANIMAL newAnimal = getAnimal();
                int successfullyAdded = addAnimal(&newAnimal, animalArray);                
                printMessageOnAdd(successfullyAdded);
                break;
            case 3: //Remove Animal
                 printf ("remove Animal\n");
                 
                 showAnimals(animalArray);
                 getName(name);
            
                 int numberOfRemovedAnimals = removeAllAnimalsBy(name, animalArray);
				 printMessageOnRemoval(numberOfRemovedAnimals);
                 break;
            case 4: //find by name
                 printf ("find by name\n");
                 
                 getName(name);
                 int index = findAnimal(name, animalArray);
                 
                 if (index >= 0)
                 {
					 printAnimal(&animalArray[index]);
				 }
				 else
				 {
					 printf("Could not find the animal named: %s\n", name);
				 } 
                 break;
			case 5:
				printf("number of animals \n");
				
				int numberOfAnimals = getNumberOfAnimals(animalArray);
				printf("There are currently %i animal(s) in the shelter.\n", numberOfAnimals);
				
				break;
			case 6:
				printf("room for one more \n");
				
				int isRoomForOneMore = isThereRoomForOneMore(animalArray);
				
				if (isRoomForOneMore == 1)
				{
					printf("There is room for at least one more animal.\n");
				}
				else
				{
					printf("There is no more room in the shelter.\n");
				}
				
				break;
            case 0:
                break;
            default:
                printf ("ERROR: invalid choice: %d\n", choice);
                break;
		}
	}
    return 0;
}
	
ANIMAL getAnimal()
{  
	ANIMAL animal;
	
	getName(animal.Name);
	animal.Species = getSpecies();
	animal.Age = getAge();
	
	return animal;
}

void getName(char *name)
{	
	printf("Please enter a name for the animal: ");
	scanf("%s", name);
	printf("\n");
}

int getSpecies()
{
	int species = -1;
	char input[1];
	
	while (species < 1 || species > NUMBER_OF_SPECIES)
	{
		printf("\nPlease select an available species\n");
		
		for (int i = 0; i < NUMBER_OF_SPECIES; i++)
		{
			printf("Enter %i for %s\n", i + 1, getSpeciesName(i));
		}
		
		printf("Enter species: ");
		scanf("%s", input);
		
		// For species 0 does not matter as it will always return us into
		// the loop.
		species = atoi(input);
	}
	
	return species - 1;
}

int getAge()
{
	int age = -1;
	char input[3];
	
	// Age could be 0 based on invalid input, thus we compare if the
	// user specifically entered a 0 character.
	while ((age < 0 || age > 100) && strcmp(input, "0") != 0)
	{
		printf("\nPlease enter an age between 0 and 100 inclusive: ");
		scanf("%s", input);
		age = atoi(input);
	}
	
	return age;
}

void printMessageOnAdd(int successCode)
{
	char *message = "\nSuccessfully added the animal to the shelter.\n";
	
	if (successCode == -1)
	{
		message = "\nCould not add animal, be sure to remove an animal if the shelter is full.\n";
	}
	
	printf("%s", message);
}

int getRemovalIndex()
{
	int animalId = 0;
	
	while(animalId < 1 || animalId > ANIMAL_SHELTER_SIZE)
	{
		printf("Enter the number of the animal you want to remove: ");
		scanf("%i", &animalId);
		printf("\n");
	}
	
	return animalId - 1;
 }

void printMessageOnRemoval(int numberOfRemovedAnimals)
{
	if (numberOfRemovedAnimals == 0)
	{
		printf("\nCould not remove animal, be sure to enter a valid name.\n");
	}
	else 
	{
		printf("Successfully removed %i animal(s)", numberOfRemovedAnimals);
	}
}
