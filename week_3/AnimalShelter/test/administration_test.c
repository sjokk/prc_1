#include <string.h>
#include "unity.h"
#include "administration.h"

const ANIMAL DEFAULT_ANIMAL = {"", Cat, -1};
const ANIMAL DUMMY = {"Barnaby Jones", Parrot, 54};
const ANIMAL ANOTHER_DUMMY = {"Killface", Dog, 8};
const int POSITIVE_INVALID_SPECIES_INT = 56;
const int NEGATIVE_INVALID_SPECIES_INT = 100;
const int SUCCESSFULL = 0;
const int UNSUCCSSFULL = -1;
const char *EMPTY_STRING = "";
const int VALID_INDEX = 0;
const int INVALID_INDEX = -100;
const int NOT_FOUND_INDEX = -1;

void setUp(void)
{
    // This is run before EACH test
}

void tearDown(void)
{
    // This is run after EACH test
}

void test_initializeShelter()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	// Act
	initializeShelter(animalArray);
	// Assert
	TEST_ASSERT_EQUAL(ANIMAL_SHELTER_SIZE, sizeof animalArray / sizeof(ANIMAL));
}

void test_addTwoAnimalsBothAnimalsHaveIdenticalFieldValues()
{
	// Arrange
	ANIMAL localDummy = DUMMY;
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	int successFullyAdded;
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		successFullyAdded = addAnimal(&localDummy, animalArray);
		// Assert
		TEST_ASSERT_EQUAL(successFullyAdded, SUCCESSFULL);
		
		ANIMAL addedAnimal = animalArray[i];
		TEST_ASSERT_EQUAL_STRING(localDummy.Name, addedAnimal.Name);
		TEST_ASSERT_EQUAL(localDummy.Species, addedAnimal.Species);
		TEST_ASSERT_EQUAL(localDummy.Age, addedAnimal.Age);
	}
}

void test_addMoreAnimalsToShelterThanMaxSize_expectedReturnCodeNegativeOne()
{
	// Arrange
	ANIMAL localDummy = DUMMY;
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	int successFullyAdded;
	
	// <= Ensures the last iteration #i == ANIMAL_SHELTER_SIZE
	// this should lead to a return value of -1 since the last dummy was
	// not added to the shelter.
	for (int i = 0; i <= ANIMAL_SHELTER_SIZE; i++) 
	{
		successFullyAdded = addAnimal(&localDummy, animalArray);
	}
	
	// Assert
	TEST_ASSERT_EQUAL(successFullyAdded, UNSUCCSSFULL);
}

void test_sucessfullyGetSpeciesName_expectedSpeciesAsStringReturned()
{
	// Arrange, Act
	SPECIES species = Dog; 
	char *speciesName = getSpeciesName(species);
	// Assert
	TEST_ASSERT_EQUAL_STRING(speciesName, "dog");
}

void test_unsuccessfullyGetSpeciesNamePositiveInteger_expectedEmptyStringreturned()
{
	// Arrange, Act
	char *speciesName = getSpeciesName(POSITIVE_INVALID_SPECIES_INT);
	// Assert
	TEST_ASSERT_EQUAL_STRING(speciesName, EMPTY_STRING);
}

void test_unsuccessfullyGetSpeciesNameNegativeInteger_expectedEmptyStringreturned()
{
	// Arrange, Act
	char *speciesName = getSpeciesName(NEGATIVE_INVALID_SPECIES_INT);
	// Assert
	TEST_ASSERT_EQUAL_STRING(speciesName, EMPTY_STRING);
}

void test_successFullyRemoveAnimalFromShelter_expectedReturnCodeZero()
{
	// Arrange
	ANIMAL localDummy = DUMMY;
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	addAnimal(&localDummy, animalArray);
	int successFullyRemoved = removeAnimalAt(VALID_INDEX, animalArray);
	
	// Assert the return code equals success.
	TEST_ASSERT_EQUAL(successFullyRemoved, SUCCESSFULL);
}

void test_unsuccessFullyRemoveAnimalFromShelterValidArrayIndex_expectedReturnCodeNegativeOne()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	int successFullyRemoved = removeAnimalAt(VALID_INDEX, animalArray);
	
	
	// Assert
	TEST_ASSERT_EQUAL(successFullyRemoved, UNSUCCSSFULL);
}

void test_unsuccessFullyRemoveAnimalFromShelterInvalidArrayIndex_expectedReturnCodeNegativeOne()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	int successFullyRemoved = removeAnimalAt(INVALID_INDEX, animalArray);
	
	
	// Assert
	TEST_ASSERT_EQUAL(successFullyRemoved, UNSUCCSSFULL);
}

// Tests for finding the animal.
void test_successfullyFindDummyAnimalIndex_expectedDummyObjectReturned()
{
	// Arrange
	ANIMAL localDummy = DUMMY;
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	addAnimal(&localDummy, animalArray);
	int index = findAnimal(localDummy.Name, animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL_STRING(localDummy.Name, animalArray[index].Name);
	TEST_ASSERT_EQUAL(localDummy.Species, animalArray[index].Species);
	TEST_ASSERT_EQUAL(localDummy.Age, animalArray[index].Age);
}

void test_unsuccessfullyFindAnimalEmptyStringSearch_expectedReturnValueNegativeOne()
{	
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	int index = findAnimal("", animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL(NOT_FOUND_INDEX, index);
}

void test_unsuccessfullyFindAnimalNonExistingNameSearch_expectedReturnValueNegativeOne()
{	
	// Arrange
	char *nonExistingName = "derpy";
	ANIMAL localDummy = DUMMY;
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	// Act
	addAnimal(&localDummy, animalArray);
	int index = findAnimal(nonExistingName, animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL(NOT_FOUND_INDEX, index);
}

void test_addMultipleAnimalsWithIdenticalName_expectedAnimalsToBeRemoved()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	ANIMAL localDummy = DUMMY;
	
	char localDummyName[64];
	strcpy(localDummyName, DUMMY.Name);
	
	ANIMAL anotherLocalDummy = ANOTHER_DUMMY;
	
	// Act
	addAnimal(&localDummy, animalArray);
	addAnimal(&anotherLocalDummy, animalArray);
	addAnimal(&localDummy, animalArray);
	
	removeAllAnimalsBy(localDummyName, animalArray);
	
	// Assert that all names are different since we expect one different name
	// and all others default animals with "" as name.
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		TEST_ASSERT_NOT_EQUAL(localDummyName, animalArray[i].Name);
	}
}

void test_fillShelterWithAnimalsWithIdenticalName_expectedAllAnimalsToBeRemoved()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	ANIMAL localDummy = DUMMY;
	
	char localDummyName[64];
	strcpy(localDummyName, DUMMY.Name);
	
	// Act
	addAnimal(&localDummy, animalArray);
	addAnimal(&localDummy, animalArray);
	addAnimal(&localDummy, animalArray);
	
	removeAllAnimalsBy(localDummyName, animalArray);
	
	// Assert that all names are different since we expect only
	// default animals to be left in the array thus having "" as name.
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		TEST_ASSERT_NOT_EQUAL(localDummyName, animalArray[i].Name);
	}
}

void test_addTwoAnimalsToTheShelter_expectedTwoAnimalsInShelter()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	ANIMAL localDummy = DUMMY;
	
	int numberOfAnimalsAddedToShelter = 2;
	// Act
	addAnimal(&localDummy, animalArray);
	addAnimal(&localDummy, animalArray);
	int numberOfAnimalsInShelter = getNumberOfAnimals(animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL(numberOfAnimalsAddedToShelter, numberOfAnimalsInShelter);
}

void test_emptyShelter_expectedZeroAnimalsInShelter()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	
	int numberOfAnimalsAddedToShelter = 0;
	
	// Act
	int numberOfAnimalsInShelter = getNumberOfAnimals(animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL(numberOfAnimalsAddedToShelter, numberOfAnimalsInShelter);
}

void test_notFullShelter_expectedTrueReturned()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	ANIMAL localDummy = DUMMY;
	
	int true = 1;
	// Act
	addAnimal(&localDummy, animalArray);
	int isRoomLeft = isThereRoomForOneMore(animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL(true, isRoomLeft);
}

void test_fullShelter_expectedFalseReturned()
{
	// Arrange
	ANIMAL animalArray[ANIMAL_SHELTER_SIZE];
	initializeShelter(animalArray);
	ANIMAL localDummy = DUMMY;
	
	int false = 0;
	// Act
	for (int i = 0; i < ANIMAL_SHELTER_SIZE; i++)
	{
		addAnimal(&localDummy, animalArray);
	}
	
	int isRoomLeft = isThereRoomForOneMore(animalArray);
	
	// Assert
	TEST_ASSERT_EQUAL(false, isRoomLeft);
}

int main (void)
{    
    UnityBegin();

	RUN_TEST(test_initializeShelter,1);
	RUN_TEST(test_addTwoAnimalsBothAnimalsHaveIdenticalFieldValues, 2);
	RUN_TEST(test_addMoreAnimalsToShelterThanMaxSize_expectedReturnCodeNegativeOne, 3);
	RUN_TEST(test_sucessfullyGetSpeciesName_expectedSpeciesAsStringReturned, 4);
	RUN_TEST(test_unsuccessfullyGetSpeciesNamePositiveInteger_expectedEmptyStringreturned, 5);
	RUN_TEST(test_unsuccessfullyGetSpeciesNameNegativeInteger_expectedEmptyStringreturned, 6);
	RUN_TEST(test_unsuccessFullyRemoveAnimalFromShelterValidArrayIndex_expectedReturnCodeNegativeOne, 7);
	RUN_TEST(test_unsuccessFullyRemoveAnimalFromShelterInvalidArrayIndex_expectedReturnCodeNegativeOne, 8);
	RUN_TEST(test_unsuccessfullyFindAnimalEmptyStringSearch_expectedReturnValueNegativeOne, 9);
	RUN_TEST(test_unsuccessfullyFindAnimalNonExistingNameSearch_expectedReturnValueNegativeOne, 10);
	RUN_TEST(test_addMultipleAnimalsWithIdenticalName_expectedAnimalsToBeRemoved, 11);
	RUN_TEST(test_fillShelterWithAnimalsWithIdenticalName_expectedAllAnimalsToBeRemoved, 12);
	RUN_TEST(test_addTwoAnimalsToTheShelter_expectedTwoAnimalsInShelter, 13);
	RUN_TEST(test_emptyShelter_expectedZeroAnimalsInShelter, 14);
	RUN_TEST(test_notFullShelter_expectedTrueReturned, 15);
	RUN_TEST(test_fullShelter_expectedFalseReturned, 16);
    
    return UnityEnd();
}
